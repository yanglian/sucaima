class testModel {

    static async getUserNum(ctx) {
        let count = await ctx.db.query('SELECT count(id) AS count FROM sc_user');
        return count[0].count;
    }
}

module.exports = testModel;