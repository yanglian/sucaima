const Router = require('koa-router');
//加载配置
const config = require('../config');
//加载checkToken中间件
const checkToken = require('../middleware/checkToken');

//加载控制器
const testController = require('../controller/test');

//加载验证器
const testValidator = require('../validator/test');


//实例化路由
router = new Router({
    prefix: config.app.baseApi
});

/* HTTP动词
    GET     //查询
    POST    //创建
    PUT     //更新
    DELETE  //删除
*/

router.get('/test', testValidator, testController.index);
router.get('/test/verify', testController.verify);
router.post('/test/verify', testController.verify);
router.get('/test/html', testController.html);
router.post('/test/upload', testController.upload);

router.get('/captcha', testController.captcha);

//404
router.get('*', async(ctx, next) => {
    ctx.error('Not Found', 404);
});

module.exports = router;