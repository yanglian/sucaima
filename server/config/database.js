module.exports = {
    mysql: {
        host: '127.0.0.1',
        port: 3306,
        user: 'root',
        password: '',
        database: 'sucaima',
        tablePrefix: 'sc_',
        charset: 'UTF8_GENERAL_CI',
        logSql: false
    }
}