const fs = require('fs');

let config = {
    jwt: {
        secret: '&*@31&#2',
        exprisesIn: '3600s' //以秒为单位
    },
    app: {
        port: process.env.PORT || 8000,
        baseApi: ''
    },
    upload: {
        fileType: 'common',
        filePath: './static/uploads/', //文件上传路径
        fileSize: 2097152, //允许上传文件大小，单位字节
        files: 10 //允许批量上传文件最大数量
    }
}

//加载数据库配置
if (fs.existsSync(__dirname + '/database.js')) {
    config = Object.assign(config, require('./database.js'));
}

module.exports = config;