const validator = require('validator');

class validate {

    constructor(data) {
        this.data = data;
        this.key = null;
        this.error = {};
    }

    name(key) {
        if (key) {
            if (key in this.data) {
                this.key = key;
            } else {
                this.key = null;
            }
        }
        return this;
    }

    empty(msg) {
        if (this.key && msg) {
            if (validator.isEmpty(this.data[this.key])) {
                this.error[this.key] = msg;
            }
        }
        return this;
    }

    email(msg) {
        if (this.key && msg) {
            if (!validator.isEmail(this.data[this.key])) {
                this.error[this.key] = msg;
            }
        }
        return this;
    }

    decimal(str) {

    }

    trim(chars) {
        if (this.key) {
            this.data[this.key] = validator.trim(this.data[this.key], chars);
        }
        return this;
    }


}

module.exports = validate;