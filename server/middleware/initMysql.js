const mysql = require('node-mysql-promise');
const config = require('../config');

module.exports = async(ctx, next) => {
    try {
        const db = await mysql.createConnection(config.mysql);
        ctx.db = db;
    } catch (err) {
        ctx.throw(501, err.message);
    }
    await next();
};