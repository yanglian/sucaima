//统一响应格式
module.exports = async(ctx, next) => {
    //请求成功时
    ctx.success = (msg, data) => {
        ctx.body = { code: 0, msg, data };
    };
    //请求失败时
    ctx.error = (msg, code = -1) => {
        // if (code) {
        //     ctx.status = code;
        // }
        ctx.body = { code, msg };
    };
    await next();
};