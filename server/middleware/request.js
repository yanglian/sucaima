const cache = require('memory-cache');
// 简化常用操作
module.exports = async(ctx, next) => {
    //挂载get数据
    ctx.$get = ctx.query;
    //挂载post数据
    ctx.$post = ctx.request.body;
    //合并get和post数据
    ctx.$param = Object.assign(ctx.$get, ctx.$post);
    //挂载cache数据
    ctx.$cache = cache;
    await next();
};