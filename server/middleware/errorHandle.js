//统一错误处理中间件，用来统一捕获其他中间件的错误,在其他中间件使用之前使用 
const tracer = require('tracer');
const moment = require('moment');
const fs = require('fs');
//自定义错误输出格式
const logger = tracer.colorConsole({
    level: 'error',
    format: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})",
    dateformat: "HH:MM:ss.L",
    transport: function(data) { //指定错误输出的文件，存到error.log下
        let date = moment().format('YYYYMMDD');
        let logPath = './runtime/log/' + date;
        if (!fs.existsSync(logPath)) {
            fs.mkdirSync(logPath);
        }
        fs.appendFile(logPath + '/error.log', data.output + '\n\n', { encoding: 'utf8' }, (err) => {
            if (err) {
                throw err;
            };
        });
    }
});
module.exports = async(ctx, next) => {
    try {
        await next();
    } catch (err) {
        logger.error(err.stack);
        throw (err);
    }
};