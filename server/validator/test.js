const Validate = require('../util/validate');

module.exports = async(ctx, next) => {
    let validate = new Validate(ctx.$param);
    validate.name('sign').empty('签名不能为空').trim();
    validate.name('email').email('邮箱地址格式不正确').trim();
    ctx.body = validate.error;
    //await next();
};